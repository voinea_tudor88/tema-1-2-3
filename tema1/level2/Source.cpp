#include <iostream>
#include "Vehicle.h"
#include <map>

int main() {

	Vehicle car1("Dacia", 2007, 1000, false);
	Vehicle car2("Skoda", 2010, 25000, true);
	Vehicle car3("Vw", 2014, 6000, true);
	Vehicle car4("Audi", 2013, 8000, true);
	Vehicle car5("Mercedes-Benz", 2018, 2500, false);

	



	std::map<Vehicle, std::string> vehicleMap;
	vehicleMap.insert(std::make_pair(car1, car1.getBrand()));
	vehicleMap.insert(std::make_pair(car2, car2.getBrand()));
	vehicleMap.insert(std::make_pair(car3, car3.getBrand()));
	vehicleMap.insert(std::make_pair(car4, car4.getBrand()));
	vehicleMap.insert(std::make_pair(car5, car5.getBrand()));

	for (auto& element : vehicleMap) {
		std::cout << element.first<< element.second.data();
	}

	system("pause");
	return 0;
}