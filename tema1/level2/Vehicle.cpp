#include "Vehicle.h"



Vehicle::Vehicle(std::string Brand, int Year, float Price, bool FullOption)
{
	this->price = Price;
	this->year = Year;
	this->price = Price;
	this->fullOption = FullOption;

}

Vehicle::~Vehicle()
{
}

std::string Vehicle::getBrand()
{
	return this->brand;
}

bool operator<(const Vehicle & first, const Vehicle & second)
{
	if (first.price - second.price > 1000)
	{
		return first.price < second.price;
	}
	if (first.fullOption == true && second.fullOption == false)
	{
		return first.fullOption < second.fullOption;
	}
		if (first.year > second.year) {

			return first.year < second.year;
		}
}


std::ostream & operator<<(std::ostream & os, const Vehicle & other)
{
	os << other.brand.data() << " " << other.year << " " << other.fullOption << " " << other.price << "\n";

	return os;
}
