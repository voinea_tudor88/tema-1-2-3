#pragma once

#include <vector>

template <typename T>
class Utils
{
public:
	struct Comparator
	{
		bool operator()(const Utils & left, const Utils & right) const
		{
			return left.mv_Value < right.mv_Value;
		}
	};

	Utils() = default;

	auto mf_MaxElement(T lv_FirstElement, T lv_SecondElement) const -> T
	{
		return lv_FirstElement > lv_SecondElement ? lv_FirstElement : lv_SecondElement;
	}

	~Utils() = default;

	std::vector<T> templateVector;

	float getMean() {
		float mean = 0;
		for (int i = 0; i < templateVector.size(); ++i) {
			mean += templateVector[i];
		}

		return mean / templateVector.size();

	}

private:
	T mv_Value;
};