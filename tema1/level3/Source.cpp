#include"Cat.h"
#include "Dog.h"
#include "PetShop.h"
#include <boost/variant.hpp>

// C++ template to print map container elements 
template <typename T, typename S>
std::ostream& operator<<(std::ostream& os, const std::map<T, S>& v)
{
	for (auto it : v)
		os << it.first << " : "
		<< it.second << "\n";

	return os;
}

int main() {

	std::shared_ptr<Dog> dog = std::make_shared<Dog>();
	std::shared_ptr<Cat> cat = std::make_shared<Cat>();

	dog.get()->name = "rex";
	//std::cout<<dog.get()->name.data();
	cat.get()->name = "miau miau";
	std::cout << dog.get() << std::endl;
	std::cout << cat.get() << std::endl;

	PetShop petshop;
	//using Key = boost::variant<char, std::string>;
	petshop.insertAnimal(dog,3);
	petshop.insertAnimal(cat,4);
	
	//std::cout << petshop.animalsMap;
	//std::cout << petshop.animalsMap << petshop.animalsMap[1];

	for (auto it : petshop.animalsMap)
	{
		std::cout << it.first<< " : "
		<< it.second << "\n";
	}
		//boost::get<int>(it.first);

	std::cin.get();
}