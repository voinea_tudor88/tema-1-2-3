#pragma once
#include <boost/variant.hpp>
#include <iostream>
#include <map>
#include "Dog.h";
#include "Cat.h"


class PetShop
{
public:
	PetShop();
	~PetShop();
public:
	std::map< boost::variant<std::shared_ptr<Dog>, std::shared_ptr<Cat>>,int> animalsMap;

	void insertAnimal(boost::variant<std::shared_ptr<Dog>, std::shared_ptr<Cat>> animal, int primitiva);

};

