#include "WalletSolution.h"



WalletSolution::WalletSolution()
{
}


WalletSolution::~WalletSolution()
{
}

void WalletSolution::addMoney(const int sum)
{
	std::mutex mtx;
	
	for (int i = 0; i < sum; i++)
	{
		money++;

		std::lock_guard<std::mutex> lck(mtx);

		std::chrono::milliseconds time(1000);
		std::this_thread::sleep_for(time);

	}
}

int WalletSolution::getMoney()
{
	return money;
}