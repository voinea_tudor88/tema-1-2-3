#include <iostream>
#include "WalletProblem.h"
#include "WalletSolution.h"

int main()
{
	WalletProblem walletProblem;


	// create a vector of threads
	std::vector<std::thread> vec;

	// int the vector
	for (auto i = 0; i < 100; ++i) {
		vec.push_back(std::thread(&WalletProblem::addMoney, walletProblem, 1));
	}

	// join all threads (maybe using mem_fn)
	std::for_each(std::begin(vec), std::end(vec), std::mem_fn(&std::thread::join));

	std::cout << walletProblem.getMoney()<<"\n";
	if (walletProblem.getMoney() == 100) std::cout << "corect";
	else std::cout << "gresit";
	



	WalletSolution walletSolution;

	std::vector<std::thread> vec2;

	// int the vector
	for (auto i = 0; i < 100; ++i) {
		vec2.push_back(std::thread(&WalletProblem::addMoney, walletProblem, 1));
	}

	// join all threads (maybe using mem_fn)
	std::for_each(std::begin(vec2), std::end(vec2), std::mem_fn(&std::thread::join));

	std::cout << walletSolution.getMoney() << "\n";
	if (walletSolution.getMoney() == 100) std::cout << "corect";
	else std::cout << "gresit";

	return 0;
}