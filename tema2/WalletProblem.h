#pragma once
#include <iostream>
#include <future>
#include <thread>

class WalletProblem
{
public:
	WalletProblem();
	~WalletProblem();

	void addMoney(const int sum);
	int getMoney();

private:
	int money = 0;
};


