#pragma once
#include <iostream>
#include <future>
#include <thread>
#include <mutex>

class WalletSolution
{
public:
	WalletSolution();
	~WalletSolution();

	void addMoney(const int sum);
	int getMoney();

private:
	int money = 0;
};

